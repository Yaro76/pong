/*
gulpfile.js

gulpfile for app web app.
*/

const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const liveload = require('gulp-livereload');
const sass = require('gulp-sass');
let jsdoc = require('gulp-jsdoc3');

gulp.task('sass', function() {
    gulp.src('./src/public/css/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./src/public/css'))
        .pipe(liveload());
});

gulp.task('js', function() {
    gulp.src('./src/public/js/client.js')
        .pipe(gulp.dest('./src/public/js/dist'))
        .pipe(liveload());
});

gulp.task('watch', function() {
    gulp.watch('./src/public/css/*.scss', ['sass']);
});

gulp.task('watch', function() {
    gulp.watch('./src/public/js/*.js', ['js']);
});

gulp.task('watch', function() {
    gulp.watch('./src/**/*.js', ['js']);
});

gulp.task('watch', function() {
    gulp.watch('./src/**/*.js', ['jsdoc']);
});

gulp.task('develop', function() {
    liveload.listen();
    nodemon({
        script: 'src/server/server.js',
        ext: 'js handlebars hbs',
        stdout: false,
    }).on('readable', function() {
        this.stdout.on('data', function(chunk) {
            if (/^Express started on/.test(chunk)) {
                liveload.changed(__dirname);
            }
        });
        this.stdout.pipe(process.stdout);
        this.stderr.pipe(process.stderr);
    });
});

gulp.task('jsdoc', function(cb) {
    let config = require('./jsdoc.json');
    gulp.src(['README.md', './src/public/**/*.js', './src/server/*.js'], {
            read: false,
        })
        .pipe(jsdoc(config, cb));
});

gulp.task('default', ['sass', 'js', 'jsdoc', 'develop', 'watch']);
